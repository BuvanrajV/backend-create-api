const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  dbPort: process.env.DB_PORT,
  host: process.env.HOST,
  user: process.env.DB_USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  port: process.env.PORT||3010
};
