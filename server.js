const express = require("express");
const cors =require("cors")
const bodyParser = require("body-parser");
const routes = require("./routes/routes");
const {port} = require("./config")

const app = express();

app.use(bodyParser.json());
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);
app.get("/*",(req,res)=>{
  res.send("Welcome to CRUD Api : To get Names - /getNames , To post Name - /postName , To update Name - /:id , To delete Name - /:id")
})

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
